# Indicina Assignment - Senior Frontend Engineer

## 🚀 Quick start

1.  **Install dependencies.**

    Navigate into your project directory and install it.

    ```shell
    # Install wtih npm
    npm install
    ```

2.  **Start developing.**

    Development mode.

    ```shell
    # run the dev server
    npm run dev
    ```

3.  **Test the code.**

    ```shell
    # exports to out folder
    npm run test
    ```

4.  **Export static site.**

    Export mode.

    ```shell
    # exports to out folder
    npm run export
    ```
