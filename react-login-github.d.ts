declare module 'react-login-github' {
  import React from 'react'

  interface Props {
    onSuccess?: any
    className: string
    clientId: string
    buttonText?: string
  }

  declare const LoginGithub: React.SFC<Props>

  export default LoginGithub
}
