import { ApolloClient, createHttpLink, InMemoryCache } from '@apollo/client'
import { setContext } from '@apollo/client/link/context'
import { getAccessToken } from './src/utils/getAccessToken'

let client: ApolloClient<any>

export const getAppoloClient = (code: string) => {
  const httpLink = createHttpLink({
    uri: 'https://api.github.com/graphql'
  })

  const authLink = setContext(async (_, { headers }) => {
    const token = await getAccessToken(code as string)
    return {
      headers: {
        ...headers,
        authorization: `Bearer ${token}`
      }
    }
  })

  client = new ApolloClient({
    link: authLink.concat(httpLink),
    cache: new InMemoryCache()
  })
  return client
}

export { client }
