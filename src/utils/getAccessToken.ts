export const getAccessToken = async (code: string): Promise<string> => {
  return localStorage.getItem('access_token') || (await fetchAccessToken(code))
}

const fetchAccessToken = async (code: string) => {
  const url = 'https://9uj0ihoex6.execute-api.eu-west-1.amazonaws.com/dev/auth'
  const response = await fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ code })
  })
  const { data }: { data: { access_token: string } } = await response.json()
  const { access_token } = data
  localStorage.setItem('access_token', access_token)
  return access_token
}
