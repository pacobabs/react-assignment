import { getAccessToken } from '../getAccessToken'

const url = 'https://9uj0ihoex6.execute-api.eu-west-1.amazonaws.com/dev/auth'

const payload = {
  body: JSON.stringify({ code: '1234' }),
  headers: { 'Content-Type': 'application/json' },
  method: 'POST'
}

describe('getAccessToken', () => {
  it('gets local token', async () => {
    ;(localStorage.getItem as jest.Mock).mockImplementationOnce(() => 'token')
    const token = await getAccessToken('1234')
    expect(localStorage.getItem).toBeCalledWith('access_token')
    expect(token).toEqual('token')
  })
  it('fetches access token', async () => {
    ;(localStorage.getItem as jest.Mock).mockImplementationOnce(() => null)
    const token = await getAccessToken('1234')
    expect(localStorage.getItem).toBeCalledWith('access_token')
    expect(fetch).toHaveBeenCalledWith(url, payload)
    expect(token).toEqual('token')
  })
})
