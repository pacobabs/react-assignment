import { roundNumber, formatNumber, getRelativeTime } from '../index'

describe('roundNumber', () => {
  it('returns the same number as string when number < 1000', async () => {
    const number = roundNumber(500)
    expect(number).toEqual('500')
  })
  it('returns the formatted number as string when number >= 1000', async () => {
    const number = roundNumber(1000)
    expect(number).toEqual('1k')
  })
})

describe('formatNumber', () => {
  it('formatNumber the number to locale number as string', async () => {
    const number = formatNumber(1000)
    expect(number).toEqual('1,000')
  })
})

describe('getRelativeTime', () => {
  it('gets the actual relative time', async () => {
    const time = getRelativeTime(new Date().toString())
    expect(time).toBeOneOf(['now', '1 second ago'])
  })
  it('gets the relative time in seconds', async () => {
    const time = getRelativeTime(new Date(new Date().getTime() - 1000).toString())
    expect(time).toBeOneOf(['1 second ago', '2 seconds ago'])
  })
  it('gets the relative time in minutes', async () => {
    const time = getRelativeTime(new Date(new Date().getTime() - 60000).toString())
    expect(time).toEqual('1 minute ago')
  })
  it('gets the relative time in hours', async () => {
    const time = getRelativeTime(new Date(new Date().getTime() - 60000 * 60).toString())
    expect(time).toEqual('1 hour ago')
  })
  it('gets the relative time in days', async () => {
    const time = getRelativeTime(new Date(new Date().getTime() - 60000 * 60 * 24).toString())
    expect(time).toEqual('yesterday')
  })
  it('gets the relative time in weeks', async () => {
    const time = getRelativeTime(new Date(new Date().getTime() - 60000 * 60 * 24 * 7).toString())
    expect(time).toEqual('last week')
  })
  it('gets the relative time in months', async () => {
    const time = getRelativeTime(new Date(new Date().getTime() - 60000 * 60 * 24 * 31).toString())
    expect(time).toEqual('last month')
  })
  it('gets the relative time in years', async () => {
    const time = getRelativeTime(new Date(new Date().getTime() - 60000 * 60 * 24 * 31 * 12).toString())
    expect(time).toEqual('last year')
  })
})
