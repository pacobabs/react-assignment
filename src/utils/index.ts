export const roundNumber = (num: number) => {
  const SUFFIXES = ['', 'k', 'M', 'G', 'T', 'P', 'E']
  const index = (Math.log10(Math.abs(num)) / 3) | 0
  return index === 0 ? num + SUFFIXES[index] : Math.floor(num / Math.pow(10, index * 3)) + SUFFIXES[index]
}

export const formatNumber = (num: number) => new Intl.NumberFormat('en').format(num)

export const getRelativeTime = (date: string) => {
  const moment = new Intl.RelativeTimeFormat('en', { numeric: 'auto' })
  const remainingTime = new Date(date).getTime() - Date.now()
  let time = (remainingTime / 1000) * -1
  let unit = 'second' as Intl.RelativeTimeFormatUnit
  if (time > 60) {
    time /= 60
    unit = 'minute'
  } else return moment.format(Math.round(-time), unit)
  if (time > 60) {
    time /= 60
    unit = 'hour'
  } else return moment.format(Math.round(-time), unit)
  if (time > 24) {
    time /= 24
    unit = 'day'
  } else return moment.format(Math.round(-time), unit)
  if (time > 7) {
    time /= 7
    unit = 'week'
  } else return moment.format(Math.round(-time), unit)
  if (time > 4) {
    time /= 4
    unit = 'month'
  } else return moment.format(Math.round(-time), unit)
  if (time > 12) {
    time /= 12
    unit = 'year'
  }
  return moment.format(Math.round(-time), unit)
}
