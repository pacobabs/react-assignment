import { formatNumber, getRelativeTime, roundNumber } from '../utils'
import { RepositoriesQuery } from '../queries/types'
import Pagination from './Pagination'
import { useEffect, useState } from 'react'

const Repositories: React.FC<Props> = ({ searchTerm, results, fetchMore }) => {
  const [currentPage, setCurrentPage] = useState(1)

  useEffect(() => {
    setCurrentPage(1)
  }, [searchTerm])

  if (!results) return null

  console.log(results)

  const { search } = results
  const { repositoryCount, repos, pageInfo } = search
  const { hasNextPage } = pageInfo

  return repositoryCount ? (
    <>
      <h1 className="mb-1 text-xl font-bold">
        {formatNumber(repositoryCount)} repository
        {repositoryCount > 1 ? ' results' : ' result'}
      </h1>
      {repos.slice((currentPage - 1) * 10, currentPage * 10).map(({ repo }) => {
        const { nameWithOwner, description, stargazers, licenseInfo, primaryLanguage, updatedAt } = repo
        return (
          <div className="p-5 bg-white" key={nameWithOwner}>
            <div className="flex gap-4">
              <h2 className="font-bold">{nameWithOwner}</h2>
            </div>
            <p className="mt-1 text-sm text-gray-500">{description}</p>
            <p className="flex gap-2 mt-4 text-xs text-gray-500">
              {stargazers.totalCount ? <span>{roundNumber(stargazers.totalCount)} Stars | </span> : null}
              {primaryLanguage ? <span>{primaryLanguage.name} | </span> : null}
              {licenseInfo ? <span>{licenseInfo.name} | </span> : null}
              {updatedAt ? <span>Updated {getRelativeTime(updatedAt)}</span> : null}
            </p>
          </div>
        )
      })}
      <Pagination
        canFetchMore={hasNextPage}
        shouldFetchMore={currentPage * 10 === repos.length}
        currentPage={currentPage}
        setCurrentPage={setCurrentPage}
        count={repositoryCount}
        fetchMore={fetchMore}
      />
    </>
  ) : (
    <h1 className="mb-1 text-xl font-bold">No results</h1>
  )
}

type Props = {
  searchTerm: string
  fetchMore: () => Promise<void>
  results?: RepositoriesQuery
}

export default Repositories
