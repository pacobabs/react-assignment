import { useQuery } from '@apollo/client'
import { useState } from 'react'
import { RepositoriesQuery, SearchType, UsersQuery } from '../queries/types'
import { roundNumber } from '../utils'
import { SEARCH_REPOSITORY, SEARCH_USER } from '../queries'
import Profile from './Profile'
import Repositories from './Repositories'
import Users from './Users'

const Results: React.FC<Props> = ({ searchTerm, handleSearch, clearSearch, logout, profileName, avatarUrl }) => {
  const [searchType, setSearchType] = useState(SearchType.Repositories)

  const { data: usersResults, fetchMore: fetchMoreUsers } = useQuery<UsersQuery>(SEARCH_USER, {
    variables: { query: searchTerm }
  })

  const { data: repositoryResults, fetchMore: fetchMoreRepositories } = useQuery<RepositoriesQuery>(SEARCH_REPOSITORY, {
    variables: { query: searchTerm }
  })

  const handleFetchMoreRepositories = async () => {
    if (repositoryResults) {
      const { search } = repositoryResults
      const { repos } = search
      const { cursor: lastCursor } = repos[repos.length - 1]
      await fetchMoreRepositories({
        variables: { query: searchTerm, after: lastCursor },
        updateQuery: (previousResults, { fetchMoreResult }) => {
          if (!fetchMoreResult) return previousResults
          return {
            ...previousResults,
            search: {
              ...fetchMoreResult.search,
              repos: [...previousResults.search.repos, ...fetchMoreResult.search.repos]
            }
          }
        }
      })
    }
  }

  const handleFetchMoreUsers = async () => {
    if (usersResults) {
      const { search } = usersResults
      const { users } = search
      const { cursor: lastCursor } = users[users.length - 1]
      await fetchMoreUsers({
        variables: { query: searchTerm, after: lastCursor },
        updateQuery: (previousResults, { fetchMoreResult }) => {
          if (!fetchMoreResult) return previousResults
          return {
            ...previousResults,
            search: {
              ...fetchMoreResult.search,
              users: [...previousResults.search.users, ...fetchMoreResult.search.users]
            }
          }
        }
      })
    }
  }

  return (
    <div className="relative min-h-screen bg-gray-100">
      <div className="flex justify-between px-[15%] py-1 gap-10 bg-white items-center h-20 sticky top-0 border-b border-gray-200 shadow-sm">
        <img
          onClick={clearSearch}
          src="/github.png"
          className="w-[170px] object-contain cursor-pointer"
          alt="github logo"
        />
        <div className="relative max-w-[380px] w-full rounded-3xl">
          <input
            autoFocus
            type="search"
            value={searchTerm}
            onChange={handleSearch}
            className="w-full h-10 px-4 border border-gray-400 outline-none rounded-3xl"
            placeholder="Search..."
          />
          {searchTerm ? null : (
            <svg
              className="absolute w-[18px] h-[18px] top-[11px] right-[11px]"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 18 18"
            >
              <path
                d="M14.2943 12.0034L17.5091 15.2182C18.1636 15.8727 18.1636 16.8545 17.5091 17.5091C17.1818 17.8364 16.6909 18 16.3636 18C16.0364 18 15.5455 17.8364 15.2182 17.5091L12.0034 14.2943C10.7859 15.0857 9.33302 15.5455 7.77273 15.5455C3.47997 15.5455 0 12.0655 0 7.77273C0 3.47997 3.47997 0 7.77273 0C12.0655 0 15.5455 3.47997 15.5455 7.77273C15.5455 9.33302 15.0857 10.7859 14.2943 12.0034ZM7.77273 12.7636C10.5291 12.7636 12.7636 10.5291 12.7636 7.77273C12.7636 5.01632 10.5291 2.78182 7.77273 2.78182C5.01632 2.78182 2.78182 5.01632 2.78182 7.77273C2.78182 10.5291 5.01632 12.7636 7.77273 12.7636Z"
                fill="#5C5C5C"
              />
            </svg>
          )}
        </div>
        <Profile profileName={profileName} avatarUrl={avatarUrl} logout={logout} />
      </div>
      <div className="flex pt-[30px] justify-center gap-5 px-4 z-99">
        <div className="sticky flex flex-col h-40 p-10 bg-white top-28">
          <div
            onClick={() => setSearchType(SearchType.Repositories)}
            className={`flex justify-between w-[220px] p-[10px] cursor-pointer ${
              searchType === SearchType.Repositories ? 'bg-gray-100' : ''
            }`}
          >
            <p className="text-sm text-gray-700">Repositories</p>
            {repositoryResults ? (
              <span className="px-2 pt-0.5 text-xs text-gray-700 bg-gray-300 rounded-xl font-bold">
                {roundNumber(repositoryResults.search.repositoryCount)}
              </span>
            ) : null}
          </div>
          <div
            onClick={() => setSearchType(SearchType.Users)}
            className={`flex justify-between w-[220px] p-[10px] cursor-pointer ${
              searchType === SearchType.Users ? 'bg-gray-100' : ''
            }`}
          >
            <p className="text-sm text-gray-700">Users</p>
            {usersResults ? (
              <span className="px-2 pt-0.5 text-xs text-gray-700 bg-gray-300 rounded-xl font-bold">
                {roundNumber(usersResults.search.userCount)}
              </span>
            ) : null}
          </div>
        </div>
        <div className="flex w-full max-w-[680px] flex-col gap-5 mb-16">
          {searchType === SearchType.Repositories && (
            <Repositories searchTerm={searchTerm} results={repositoryResults} fetchMore={handleFetchMoreRepositories} />
          )}
          {searchType === SearchType.Users && (
            <Users searchTerm={searchTerm} results={usersResults} fetchMore={handleFetchMoreUsers} />
          )}
        </div>
      </div>
    </div>
  )
}

type Props = {
  searchTerm: string
  handleSearch: (e: React.ChangeEvent<HTMLInputElement>) => void
  clearSearch: () => void
  logout: () => any
  profileName?: string
  avatarUrl?: string
}

export default Results
