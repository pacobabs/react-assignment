import { formatNumber } from '../utils'
import { UsersQuery } from '../queries/types'
import Pagination from './Pagination'
import { useEffect, useState } from 'react'

const Users: React.FC<Props> = ({ searchTerm, results, fetchMore }) => {
  const [currentPage, setCurrentPage] = useState(1)

  useEffect(() => {
    setCurrentPage(1)
  }, [searchTerm])

  if (!results) return null
  const { search } = results
  const { userCount, users, pageInfo } = search
  const { hasNextPage } = pageInfo

  return userCount ? (
    <>
      <h1 className="mb-1 text-xl font-bold">
        {formatNumber(userCount)} {userCount > 1 ? 'users' : 'user'}
      </h1>
      {users.slice((currentPage - 1) * 10, currentPage * 10).map(({ user }) => {
        const { id, name, login, bio, location } = user
        return (
          <div className="p-5 bg-white" key={id}>
            <div className="flex items-center gap-2">
              <h2 className="font-bold whitespace-nowrap">{name || login}</h2>
              <span className="mt-0.5 text-sm text-gray-500">{location}</span>
            </div>
            <p className="mt-1 text-xs text-gray-500">{bio}</p>
          </div>
        )
      })}
      <Pagination
        canFetchMore={hasNextPage}
        shouldFetchMore={currentPage * 10 === users.length}
        currentPage={currentPage}
        setCurrentPage={setCurrentPage}
        count={userCount}
        fetchMore={fetchMore}
      />
    </>
  ) : (
    <h1 className="mb-1 text-xl font-bold">No results</h1>
  )
}

type Props = {
  searchTerm: string
  fetchMore: () => Promise<void>
  results?: UsersQuery
}

export default Users
