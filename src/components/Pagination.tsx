import { Dispatch, SetStateAction } from 'react'

const Pagination: React.FC<Props> = ({
  count,
  fetchMore,
  currentPage,
  setCurrentPage,
  shouldFetchMore,
  canFetchMore
}) => {
  if (count <= 10) return null
  const numberOfPages = Math.floor(count / 10) + 1
  const isFirstPage = currentPage === 1
  const isLastPage = currentPage === numberOfPages

  const previousPage = () => {
    setCurrentPage((currentPage) => Math.max(1, currentPage - 1))
  }

  const nextPage = async () => {
    if (shouldFetchMore && canFetchMore) {
      await fetchMore()
    }
    setCurrentPage((currentPage) => Math.min(numberOfPages, currentPage + 1))
  }

  return (
    <div className="flex justify-end gap-4 mt-8 font-inter">
      <button
        onClick={previousPage}
        disabled={isFirstPage}
        className="p-4 text-white bg-gray-800 rounded-xl disabled:text-gray-600 disabled:bg-gray-200"
      >
        <svg className="w-4 h-4" viewBox="0 0 8 14" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M7 1L1 7L7 13" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
        </svg>
      </button>
      {Array(Math.min(5, numberOfPages))
        .fill('')
        .map((_, index) => {
          const currentPageIndex = index + (currentPage > 5 ? currentPage - 5 + 1 : 1)
          return (
            <span className={`p-2 ${currentPage !== currentPageIndex ? 'text-gray-400' : ''}`} key={index}>
              {currentPageIndex}
            </span>
          )
        })}
      {numberOfPages > 5 ? (
        <>
          {currentPage < numberOfPages - 1 ? <span className="p-2 text-gray-400">...</span> : null}
          {currentPage < numberOfPages ? <span className="p-2 text-gray-400">{numberOfPages}</span> : null}
        </>
      ) : null}
      <button
        onClick={nextPage}
        disabled={isLastPage}
        className="p-4 text-white bg-gray-800 rounded-xl disabled:text-gray-600 disabled:bg-gray-200"
      >
        <svg className="w-4 h-4" viewBox="0 0 8 14" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M1 1L7 7L1 13" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
        </svg>
      </button>
    </div>
  )
}

type Props = {
  currentPage: number
  setCurrentPage: Dispatch<SetStateAction<number>>
  fetchMore: () => Promise<void>
  shouldFetchMore: boolean
  canFetchMore: boolean
  count: number
}

export default Pagination
