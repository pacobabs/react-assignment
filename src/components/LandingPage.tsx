import React, { useState } from 'react'
import Profile from './Profile'
import Results from './Results'
import { GET_CURRENT_USER } from '../queries'
import { useQuery } from '@apollo/client'
import { useRouter } from 'next/router'
import { client } from '../../AppoloClient'
import { ViewerQuery } from '../queries/types'

const LandingPage = () => {
  const router = useRouter()
  const [searchTerm, setSearchTerm] = useState('')
  const [searchStarted, setSearchStarted] = useState(false)

  const { data, loading } = useQuery<ViewerQuery>(GET_CURRENT_USER)

  const logout = () => {
    client.clearStore()
    router.push('/')
  }

  const startSearch = () => {
    searchTerm && setSearchStarted(true)
  }

  const handleSearch = (e: React.ChangeEvent<HTMLInputElement>) => {
    setSearchTerm(e.target.value)
  }

  const clearSearch = () => {
    setSearchTerm('')
    setSearchStarted(false)
  }

  if (loading || !data) return <div data-testid="loading"></div>

  const { viewer } = data
  const { avatarUrl, name } = viewer

  return searchStarted ? (
    <Results
      searchTerm={searchTerm}
      handleSearch={handleSearch}
      clearSearch={clearSearch}
      logout={logout}
      profileName={name}
      avatarUrl={avatarUrl}
    />
  ) : (
    <div>
      <div className="flex justify-end px-[15%] py-1 h-20">
        <Profile profileName={name} avatarUrl={avatarUrl} logout={logout} />
      </div>
      <div className="flex flex-col items-center px-4">
        <img src="/github.png" className="mt-[110px]" alt="github logo" />
        <div className="relative max-w-[580px] mt-5 w-full rounded-3xl">
          <input
            autoFocus
            data-testid="input-search"
            type="search"
            value={searchTerm}
            onChange={handleSearch}
            className="w-full h-10 px-4 py-3 border border-gray-400 outline-none rounded-3xl"
          />
          {searchTerm ? null : (
            <svg
              className="absolute w-[18px] h-[18px] top-[11px] right-[11px]"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 18 18"
            >
              <path
                d="M14.2943 12.0034L17.5091 15.2182C18.1636 15.8727 18.1636 16.8545 17.5091 17.5091C17.1818 17.8364 16.6909 18 16.3636 18C16.0364 18 15.5455 17.8364 15.2182 17.5091L12.0034 14.2943C10.7859 15.0857 9.33302 15.5455 7.77273 15.5455C3.47997 15.5455 0 12.0655 0 7.77273C0 3.47997 3.47997 0 7.77273 0C12.0655 0 15.5455 3.47997 15.5455 7.77273C15.5455 9.33302 15.0857 10.7859 14.2943 12.0034ZM7.77273 12.7636C10.5291 12.7636 12.7636 10.5291 12.7636 7.77273C12.7636 5.01632 10.5291 2.78182 7.77273 2.78182C5.01632 2.78182 2.78182 5.01632 2.78182 7.77273C2.78182 10.5291 5.01632 12.7636 7.77273 12.7636Z"
                fill="#5C5C5C"
              />
            </svg>
          )}
        </div>
        <button data-testid="button-search" onClick={startSearch} className="mt-[30px] button">
          Search Github
        </button>
      </div>
    </div>
  )
}

export default LandingPage
