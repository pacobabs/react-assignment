import { useState } from 'react'

const Profile: React.FC<Props> = ({ profileName, avatarUrl, logout }) => {
  const [showMenu, setShowMenu] = useState(false)

  const toggleMenu = () => setShowMenu((showMenu) => !showMenu)

  return (
    <div className="flex items-center gap-[10px] relative">
      {avatarUrl ? (
        <img
          className="object-contain rounded-full w-[50px] h-[50px] bg-blue-gray-700"
          src={avatarUrl}
          alt="profile picture"
        />
      ) : null}
      {profileName ? (
        <div
          onClick={toggleMenu}
          data-testid="button-toggle-menu"
          className="flex items-center gap-[10px] cursor-pointer"
        >
          <p className="whitespace-nowrap font-roboto">{profileName}</p>
          <svg className="w-3 h-2 mt-0.5" viewBox="0 0 12 7" xmlns="http://www.w3.org/2000/svg">
            <path
              d="M11.7156 0.300562C11.5278 0.11125 11.2722 0.00476399 11.0056 0.00476399C10.7389 0.00476399 10.4833 0.11125 10.2956 0.300562L6.00556 4.57056L1.70556 0.290563C1.31344 -0.0987982 0.679923 -0.09656 0.290562 0.295562C-0.0987984 0.687684 -0.0965597 1.3212 0.295562 1.71056L5.29556 6.71056C5.4859 6.89409 5.74119 6.99477 6.00556 6.99056C6.2679 6.98946 6.5193 6.88531 6.70556 6.70056L11.7056 1.70056C12.0895 1.31445 12.0939 0.692123 11.7156 0.300562Z"
              fill="black"
            />
          </svg>
        </div>
      ) : null}
      {showMenu ? (
        <div className="absolute right-0 flex w-48 px-10 py-5 bg-white border border-gray-100 shadow-lg top-16 shadow-dark-100">
          <button data-testid="button-logout" className="w-full text-left text-red-500" onClick={logout}>
            Logout
          </button>
        </div>
      ) : null}
    </div>
  )
}

type Props = {
  profileName?: string
  avatarUrl?: string
  logout: () => void
}

export default Profile
