import { useRouter } from 'next/router'
import { useEffect } from 'react'
import LoginGithub from 'react-login-github'

const Login = () => {
  const router = useRouter()

  useEffect(() => {
    localStorage.removeItem('access_token')
  }, [])

  const onSuccess = async (responseCode: { code: string }) => {
    const { code } = responseCode
    router.push({
      pathname: '/search',
      query: { code }
    })
  }

  return (
    <div className="flex justify-center">
      <LoginGithub
        onSuccess={onSuccess}
        className="button mt-[42vh]"
        buttonText="Login to Github"
        clientId="4f262cc9e20d3043da02"
      />
    </div>
  )
}

export default Login
