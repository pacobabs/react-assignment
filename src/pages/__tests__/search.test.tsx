import { act, fireEvent, render, screen, waitFor } from '@testing-library/react'
import Results from '../../components/Results'
import Search from '../../pages/search'
import { MockedProvider } from '@apollo/client/testing'
import LandingPage from '../../components/LandingPage'
import { router } from '../../../__mocks__/next/router'
import mocks from '../../../__mocks__'

describe('Landing page', () => {
  it('renders and loads appolo client', async () => {
    ;(localStorage.getItem as jest.Mock).mockImplementationOnce(() => null)
    await act(async () => {
      render(<Search />)
    })
    await act(async () => {
      screen.findByTestId('loading')
    })
  })
  it('logs out', async () => {
    await act(async () => {
      render(
        <MockedProvider mocks={mocks} addTypename={false}>
          <LandingPage />
        </MockedProvider>
      )
    })
    await act(async () => {
      screen.findByTestId('loading')
      await new Promise((resolve) => setTimeout(resolve, 0))
      const toggleMenuButton = await screen.findByTestId('button-toggle-menu')
      fireEvent.click(toggleMenuButton)
      const logoutButton = await screen.findByTestId('button-logout')
      fireEvent.click(logoutButton)
      expect(router.push).toBeCalledWith('/')
    })
  })
  it('renders and shows results page', async () => {
    await act(async () => {
      render(
        <MockedProvider mocks={mocks} addTypename={false}>
          <LandingPage />
        </MockedProvider>
      )
    })
    await act(async () => {
      screen.findByTestId('loading')
      await new Promise((resolve) => setTimeout(resolve, 0))
      screen.getByText('Search Github')
      const input = await screen.findByTestId('input-search')
      fireEvent.change(input, { target: { value: '1234' } })
      const button = await screen.findByTestId('button-search')
      fireEvent.click(button)
    })
    screen.getByText('Repositories')
  })
})

describe('Results page', () => {
  it('renders', async () => {
    const handleSearch = jest.fn()
    const clearSearch = jest.fn()
    const logout = jest.fn()
    await act(async () => {
      render(
        <MockedProvider>
          <Results searchTerm="" handleSearch={handleSearch} clearSearch={clearSearch} logout={logout} />
        </MockedProvider>
      )
    })
    screen.getByText('Repositories')
  })
})
