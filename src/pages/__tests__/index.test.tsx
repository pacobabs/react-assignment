import { render, screen } from '@testing-library/react'
import Index from '../index'
import Login from '../../components/Login'

jest.mock('next/dynamic', () => ({
  __esModule: true,
  default: () => {
    return () => <Login />
  }
}))

describe('Index page', () => {
  it('renders', async () => {
    render(<Index />)
    expect(localStorage.removeItem).toBeCalledWith('access_token')
    screen.getByText('Login to Github')
  })
})
