import React, { FC } from 'react'
import Head from 'next/head'
import dynamic from 'next/dynamic'

/* istanbul ignore next */
const Login = dynamic(() => import('../components/Login'), { ssr: false })

const Index: FC = () => (
  <>
    <Head>
      <title>Indicina Assignment - Senior Frontend Engineer</title>
    </Head>
    <Login />
  </>
)

export default Index
