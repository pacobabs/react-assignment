import React, { FC } from 'react'
import { ApolloProvider } from '@apollo/client'
import { getAppoloClient } from '../../AppoloClient'
import Head from 'next/head'
import LandingPage from '../components/LandingPage'
import { useRouter } from 'next/router'

const Index: FC = () => {
  const router = useRouter()
  const { code } = router.query
  const client = getAppoloClient(code as string)
  return (
    <>
      <Head>
        <title>Indicina Assignment - Senior Frontend Engineer</title>
      </Head>
      <ApolloProvider client={client}>
        <LandingPage />
      </ApolloProvider>
    </>
  )
}

export default Index
