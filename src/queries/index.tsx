import { gql } from '@apollo/client'

export const GET_CURRENT_USER = gql`
  {
    viewer {
      name
      avatarUrl
    }
  }
`

export const SEARCH_REPOSITORY = gql`
  query SearchRepositories($query: String!, $after: String) {
    search(query: $query, type: REPOSITORY, first: 10, after: $after) {
      repositoryCount
      pageInfo {
        hasNextPage
      }
      repos: edges {
        cursor
        repo: node {
          ... on Repository {
            name
            nameWithOwner
            description
            primaryLanguage {
              name
            }
            updatedAt
            licenseInfo {
              name
            }
            stargazers {
              totalCount
            }
          }
        }
      }
    }
  }
`

export const SEARCH_USER = gql`
  query SearchUsers($query: String!, $after: String) {
    search(query: $query, type: USER, first: 10, after: $after) {
      userCount
      pageInfo {
        hasNextPage
      }
      users: edges {
        cursor
        user: node {
          ... on User {
            id
            name
            login
            bio
            location
          }
        }
      }
    }
  }
`
