export enum SearchType {
  Repositories = 'Repositories',
  Users = 'Users'
}

export type ViewerQuery = {
  viewer: {
    name: string
    avatarUrl: string
  }
}

export type RepositoriesQuery = {
  search: {
    repos: RepositoryResult[]
    repositoryCount: number
    pageInfo: {
      hasNextPage: boolean
    }
  }
}

type RepositoryResult = {
  cursor: string
  repo: Repository
}

type Repository = {
  nameWithOwner: string
  description: string
  updatedAt: string
  primaryLanguage: {
    name: string
  }
  licenseInfo: {
    name: string
  }
  stargazers: {
    totalCount: number
  }
}

export type UsersQuery = {
  search: {
    users: UserResult[]
    userCount: number
    pageInfo: {
      hasNextPage: boolean
    }
  }
}

type UserResult = {
  cursor: string
  user: User
}

type User = {
  id: string
  name: string
  login: string
  bio: string
  location: string
}
