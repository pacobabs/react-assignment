import React from 'react'
import 'jest-localstorage-mock'
import 'jest-extended'
import 'jest-extended/all'

global.fetch = jest.fn(() =>
  Promise.resolve({
    json: () => Promise.resolve({ data: { access_token: 'token' } })
  })
) as jest.Mock
