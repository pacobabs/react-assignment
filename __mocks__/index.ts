import { GET_CURRENT_USER } from '../src/queries'
import { ViewerQuery } from '../src/queries/types'

const user: ViewerQuery = {
  viewer: {
    name: 'test',
    avatarUrl: 'test'
  }
}

export default [
  {
    request: {
      query: GET_CURRENT_USER
    },
    result: {
      data: user
    }
  }
]
