export const router = { query: { code: '1234' }, push: jest.fn() }
export const useRouter = jest.fn(() => router)
